#! /bin/zsh

#
# amending sqlite3 database schema depending on schema version
#

[[ -z "${TLA_DATABASEFILE}" ]] && echo "missing env variable TLA_DATABASEFILE, aborting" && exit 9

# ver=$(sqlite3 "${TLA_DATABASEFILE}" 'select ver from schema_ver order by stamp desc limit 1;' '.quit')

# [[ $? != 0 ]] && echo "sqlite3 execution failed" && exit 9

# case $ver in
#	1)
#		sqlite3 "${TLA_DATABASEFILE}" -init './amend1.sql' '.quit'
#		echo "Upgrading database from schema 1"
#		;;
#	2)
#		sqlite3 "${TLA_DATABASEFILE}" -init './amend2.sql' '.quit'
#		echo "Upgrading database from schema 2"
#		;;
# esac

echo "sqlite3 execution completed"