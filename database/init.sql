create table log(id integer PRIMARY KEY, timestamp datetime DEFAULT CURRENT_TIMESTAMP, event varchar(255));
create table mtuser(id integer, created datetime, name varchar(255), code varchar(10), finger integer, rfid varchar(10), pin varchar(10));
create table mtpunch(id integer PRIMARY KEY, user integer, punch datetime, method varchar(20), type varchar(10), photo text, processed integer);
create table mtpunchfail(id integer PRIMARY KEY, punch datetime, method varchar(20), photo text, processed integer);
create table mtversion(ver integer, stamp datetime DEFAULT CURRENT_TIMESTAMP);
create table schema_ver(ver integer, stamp datetime DEFAULT CURRENT_TIMESTAMP);
insert into mtversion (ver) values (7000001);
insert into schema_ver (ver) values (1);
insert into mtuser (id, name, code, finger, rfid, pin) values (1, "Superuser", "SUPER", 0, null, "877415");

