import * as i0 from '@angular/core';
import { Injectable, Component, NgModule, Inject } from '@angular/core';
import { WebSocketSubject } from 'rxjs/webSocket';

class TstlaService {
    constructor() {
        console.log("NYI TstlaService");
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "18.2.13", ngImport: i0, type: TstlaService, deps: [], target: i0.ɵɵFactoryTarget.Injectable }); }
    static { this.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "18.2.13", ngImport: i0, type: TstlaService, providedIn: 'root' }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "18.2.13", ngImport: i0, type: TstlaService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: () => [] });

class TstlaComponent {
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "18.2.13", ngImport: i0, type: TstlaComponent, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "18.2.13", type: TstlaComponent, selector: "lib-tstla", ngImport: i0, template: `
    <p>
      tstla works!
    </p>
  `, isInline: true }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "18.2.13", ngImport: i0, type: TstlaComponent, decorators: [{
            type: Component,
            args: [{ selector: 'lib-tstla', template: `
    <p>
      tstla works!
    </p>
  ` }]
        }] });

class TstlaModule {
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "18.2.13", ngImport: i0, type: TstlaModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule }); }
    static { this.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "14.0.0", version: "18.2.13", ngImport: i0, type: TstlaModule, declarations: [TstlaComponent], exports: [TstlaComponent] }); }
    static { this.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "18.2.13", ngImport: i0, type: TstlaModule }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "18.2.13", ngImport: i0, type: TstlaModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        TstlaComponent
                    ],
                    imports: [],
                    exports: [
                        TstlaComponent
                    ]
                }]
        }] });

class TsevService extends WebSocketSubject {
    /** System Event types */
    static { this.evtype = {
        Browser: "sbr",
        LocalServer: "slo",
        UserMsg: "urm"
    }; }
    constructor(url = "ws://localhost.local:2000") {
        super({ url: url,
            binaryType: 'arraybuffer'
        });
        this.version = "2b";
        console.log("Now constructing TsevService");
        this.subscribe({
            next: (msg) => console.log('event received', msg),
            error: (err) => { console.log('event error', err); }, // Called if at any point there is some kind of error.
            complete: () => console.log('event complete') // Called when connection is closed (for whatever reason).
        });
    }
    toLocalServer(o1) {
        let event = {
            evtype: TsevService.evtype.LocalServer,
            evdata: o1
        };
        console.log("TsevService toLocalServer", event);
        this.next(event);
    }
    restore() {
        this["_connectSocket"]();
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "18.2.13", ngImport: i0, type: TsevService, deps: [{ token: 'url' }], target: i0.ɵɵFactoryTarget.Injectable }); }
    static { this.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "18.2.13", ngImport: i0, type: TsevService, providedIn: 'root' }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "18.2.13", ngImport: i0, type: TsevService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: () => [{ type: undefined, decorators: [{
                    type: Inject,
                    args: ['url']
                }] }] });

/*
 * Public API Surface of tstla
 */

/**
 * Generated bundle index. Do not edit.
 */

export { TsevService, TstlaComponent, TstlaModule, TstlaService };
//# sourceMappingURL=bttla.mjs.map
