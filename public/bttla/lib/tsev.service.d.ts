import { WebSocketSubject } from 'rxjs/webSocket';
import * as i0 from "@angular/core";
export declare class TsevService extends WebSocketSubject<any> {
    version: string;
    /** System Event types */
    private static readonly evtype;
    constructor(url?: string);
    toLocalServer(o1: any): void;
    restore(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<TsevService, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<TsevService>;
}
