Ts tla components for angular
=============================

# bttla library
Contains bttla library which is generated with Angular CLI version 16.2.0.
Started life in directory projects/bttla.

## Clone
Use ```git submodule add <URL> bttla``` within parent angular workspace.

## Build
Run `ng build bttla` to build the library. The build artifacts will be stored in the parent `dist/` directory.

Or use the json fragment below and paste into parent *angular.json* under *projects* and use ng build for library and app builds.

## Install ng-packagr
This command seems to install correct version
```
npm install -D ng-packagr
```

- - - 
    "bttla": {
      "projectType": "library",
      "root": "bttla",
      "sourceRoot": "bttla/src",
      "prefix": "lib",
      "architect": {
        "build": {
          "builder": "@angular-devkit/build-angular:ng-packagr",
          "options": {
            "project": "bttla/ng-package.json"
          },
          "configurations": {
            "production": {
              "tsConfig": "bttla/tsconfig.lib.prod.json"
            },
            "development": {
              "tsConfig": "bttla/tsconfig.lib.json"
            }
          },
          "defaultConfiguration": "production"
        },
        "test": {
          "builder": "@angular-devkit/build-angular:karma",
          "options": {
            "tsConfig": "bttla/tsconfig.spec.json",
            "polyfills": [
              "zone.js",
              "zone.js/testing"
            ]
          }
        }
      }
