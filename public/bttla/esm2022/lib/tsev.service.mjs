import { Inject, Injectable } from '@angular/core';
import { WebSocketSubject } from 'rxjs/webSocket';
import * as i0 from "@angular/core";
export class TsevService extends WebSocketSubject {
    /** System Event types */
    static { this.evtype = {
        Browser: "sbr",
        LocalServer: "slo",
        UserMsg: "urm"
    }; }
    constructor(url = "ws://localhost.local:2000") {
        super({ url: url,
            binaryType: 'arraybuffer'
        });
        this.version = "2b";
        console.log("Now constructing TsevService");
        this.subscribe({
            next: (msg) => console.log('event received', msg),
            error: (err) => { console.log('event error', err); }, // Called if at any point there is some kind of error.
            complete: () => console.log('event complete') // Called when connection is closed (for whatever reason).
        });
    }
    toLocalServer(o1) {
        let event = {
            evtype: TsevService.evtype.LocalServer,
            evdata: o1
        };
        console.log("TsevService toLocalServer", event);
        this.next(event);
    }
    restore() {
        this["_connectSocket"]();
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "18.2.13", ngImport: i0, type: TsevService, deps: [{ token: 'url' }], target: i0.ɵɵFactoryTarget.Injectable }); }
    static { this.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "18.2.13", ngImport: i0, type: TsevService, providedIn: 'root' }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "18.2.13", ngImport: i0, type: TsevService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: () => [{ type: undefined, decorators: [{
                    type: Inject,
                    args: ['url']
                }] }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHNldi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vYnR0bGEvc3JjL2xpYi90c2V2LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFHbkQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7O0FBTWxELE1BQU0sT0FBTyxXQUFZLFNBQVEsZ0JBQXFCO0lBR2xELHlCQUF5QjthQUNGLFdBQU0sR0FBRztRQUM1QixPQUFPLEVBQWMsS0FBSztRQUMxQixXQUFXLEVBQVUsS0FBSztRQUMxQixPQUFPLEVBQWMsS0FBSztLQUM3QixBQUo0QixDQUkzQjtJQUVKLFlBQTJCLEdBQUcsR0FBRywyQkFBMkI7UUFDMUQsS0FBSyxDQUNILEVBQUMsR0FBRyxFQUFFLEdBQUc7WUFDVCxVQUFVLEVBQUUsYUFBYTtTQUN4QixDQUNGLENBQUM7UUFkRyxZQUFPLEdBQVksSUFBSSxDQUFDO1FBZTdCLE9BQU8sQ0FBQyxHQUFHLENBQUMsOEJBQThCLENBQUMsQ0FBQztRQUM1QyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQ2IsSUFBSSxFQUFFLENBQUMsR0FBUSxFQUFFLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLEdBQUcsQ0FBQztZQUN0RCxLQUFLLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUEsQ0FBQyxFQUFFLHNEQUFzRDtZQUMzRyxRQUFRLEVBQUUsR0FBRyxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLDBEQUEwRDtTQUN6RyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU0sYUFBYSxDQUFDLEVBQVE7UUFDM0IsSUFBSSxLQUFLLEdBQUc7WUFDVixNQUFNLEVBQUcsV0FBVyxDQUFDLE1BQU0sQ0FBQyxXQUFXO1lBQ3ZDLE1BQU0sRUFBRSxFQUFFO1NBQ1QsQ0FBQztRQUNKLE9BQU8sQ0FBQyxHQUFHLENBQUMsMkJBQTJCLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNuQixDQUFDO0lBRUQsT0FBTztRQUNMLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLENBQUM7SUFDM0IsQ0FBQzsrR0FuQ1UsV0FBVyxrQkFVRixLQUFLO21IQVZkLFdBQVcsY0FGVixNQUFNOzs0RkFFUCxXQUFXO2tCQUh2QixVQUFVO21CQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQjs7MEJBV2MsTUFBTTsyQkFBQyxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgV2ViU29ja2V0U3ViamVjdCB9IGZyb20gJ3J4anMvd2ViU29ja2V0JztcclxuaW1wb3J0IHsgd2ViU29ja2V0IH0gZnJvbSAncnhqcy93ZWJTb2NrZXQnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgVHNldlNlcnZpY2UgZXh0ZW5kcyBXZWJTb2NrZXRTdWJqZWN0PGFueT4ge1xyXG4gIHB1YmxpYyB2ZXJzaW9uIDogc3RyaW5nID0gXCIyYlwiO1xyXG5cclxuICAgIC8qKiBTeXN0ZW0gRXZlbnQgdHlwZXMgKi9cclxuICAgcHJpdmF0ZSBzdGF0aWMgcmVhZG9ubHkgZXZ0eXBlID0ge1xyXG4gICAgICAgIEJyb3dzZXIgICAgICAgICAgICA6IFwic2JyXCIsXHJcbiAgICAgICAgTG9jYWxTZXJ2ZXIgICAgICAgIDogXCJzbG9cIixcclxuICAgICAgICBVc2VyTXNnICAgICAgICAgICAgOiBcInVybVwiXHJcbiAgICB9O1xyXG5cclxuICBjb25zdHJ1Y3RvcihASW5qZWN0KCd1cmwnKSB1cmwgPSBcIndzOi8vbG9jYWxob3N0LmxvY2FsOjIwMDBcIikge1xyXG4gICAgc3VwZXIoXHJcbiAgICAgIHt1cmw6IHVybCxcclxuICAgICAgYmluYXJ5VHlwZTogJ2FycmF5YnVmZmVyJ1xyXG4gICAgICB9XHJcbiAgICApO1xyXG4gICAgY29uc29sZS5sb2coXCJOb3cgY29uc3RydWN0aW5nIFRzZXZTZXJ2aWNlXCIpO1xyXG4gICAgdGhpcy5zdWJzY3JpYmUoe1xyXG4gICAgICBuZXh0OiAobXNnOiBhbnkpID0+IGNvbnNvbGUubG9nKCdldmVudCByZWNlaXZlZCcsIG1zZyksXHJcbiAgICAgIGVycm9yOiAoZXJyKSA9PiB7IGNvbnNvbGUubG9nKCdldmVudCBlcnJvcicsIGVycik7fSwgLy8gQ2FsbGVkIGlmIGF0IGFueSBwb2ludCB0aGVyZSBpcyBzb21lIGtpbmQgb2YgZXJyb3IuXHJcbiAgICAgIGNvbXBsZXRlOiAoKSA9PiBjb25zb2xlLmxvZygnZXZlbnQgY29tcGxldGUnKSAvLyBDYWxsZWQgd2hlbiBjb25uZWN0aW9uIGlzIGNsb3NlZCAoZm9yIHdoYXRldmVyIHJlYXNvbikuXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyB0b0xvY2FsU2VydmVyKG8xIDogYW55KSA6IHZvaWQge1xyXG4gICAgbGV0IGV2ZW50ID0ge1xyXG4gICAgICBldnR5cGU6ICBUc2V2U2VydmljZS5ldnR5cGUuTG9jYWxTZXJ2ZXIsXHJcbiAgICAgIGV2ZGF0YTogbzFcclxuICAgICAgfTtcclxuICAgIGNvbnNvbGUubG9nKFwiVHNldlNlcnZpY2UgdG9Mb2NhbFNlcnZlclwiLCBldmVudCk7XHJcbiAgICB0aGlzLm5leHQoZXZlbnQpO1xyXG4gIH1cclxuXHJcbiAgcmVzdG9yZSgpIDogdm9pZCB7XHJcbiAgICB0aGlzW1wiX2Nvbm5lY3RTb2NrZXRcIl0oKTtcclxuICB9XHJcblxyXG59XHJcbiJdfQ==