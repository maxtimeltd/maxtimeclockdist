//-----
// nodejs demo tla app script 
//-----

var TRACEHDR = "NODE-APPS"

/// Get a top handler to the ts-tla module
const tla = require('ts-tla');
tla.check();

// Get us a tla interface wrap
const wrap = require('./tla4node')(tla);
wrap.startup(tla);

// Get us a db layer
const mydb = require('./mydb.js')(wrap);

// Get us a finit state machine with the app logic
const fsm = require('./myfsm')(wrap, mydb);

// Require successful 
console.log(TRACEHDR + " Require done")

/// node internal process handling
process.on('SIGINT', 
    () => {
        console.log("Terminating");
        tla.closeall();
        process.exit(9);
    }
);

process.on('exit', 
    () => {
        tla.closeall();
    }        
);

// start fsm
fsm.start();

/// Start tla, dispatcher and backend pump
tla.start();

// make something happen on the log
setInterval(
    () => {fsm.tick()},
    20000
); 