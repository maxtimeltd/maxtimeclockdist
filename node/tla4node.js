//-----
// tla node interface module
// version 0.6
//
// Interfacing tla event subsystem to nodejs
// Make a copy and adapt to your needs, but be alert to new versions released.
// More info on partner area wiki.
//-----

var TRACEHDR = "NODE-ITLA "

var self = {
    //----- Main exported object
    
    disp: null,
    bkend: null,
    tdb: null,
    tla: null,

    startup: (tla) => {
        //----- Control objects, dispatcher, backend and database
        
        let disp = tla.dispatcher();
        let bkend = tla.backend();
        let tdb = tla.database();
        
        self.tla   = tla;
        self.disp  = disp;
        self.bkend = bkend;
        self.tdb   = tdb;

        //----- Define additional tla access functions

        // send any event
        // event: tla event jsobject
        self.disp.sendEvent = async function (event) {
            console.log(TRACEHDR + "sendEvent" ,event);
            await disp.send(event);
        }

        // browser send event
        // obj: any jsobject
        self.disp.toBrowser = async function (obj) {
            await disp.send(
                {evtype: tla.evtype.Browser, evdata: obj}
            );
        }

        // enroll next step send event
        self.disp.enrollNextStep = async function (obj) {
            await disp.send(
                {evtype: tla.evtype.NextStepEnroll, evdata: obj}
            );
        },

        // camera exposure
        self.disp.camCmdExposure = async function () {
            await disp.send(
                {evtype: tla.evtype.CamCmdExposure}
            );
        },

        // request a reboot [New 0.3]
        self.disp.sysopReboot = async function () {
            let sysop = tla.sysop(
                (err) => {console.error(TRACEHDR + "fail creating sysop for reboot")}
            );
            sysop.rebootGracefully();
        },

        // request a redeploy of tla app [New 0.3]
        self.disp.sysopRedeployTlaApp = async function () {
            let sysop = tla.sysop(
                (err) => {console.error(TRACEHDR + "fail creating sysop for redeploy")}
            );
            sysop.redeployTlaApp();
        }

        console.log(TRACEHDR + "startup done");
    }, // eo func
    
    eventhandlers: (tla, fsm) => {
        
        //----- Setup events handlers relaying to fsm
        
        // note: uses meta syntax not-js characters: < > | ^

        /// Handler for Init, called once
        self.disp.on(tla.evtype.Init,
            async () => {
                console.log(TRACEHDR + " on Init, events initialized")
                fsm.init();
            }
        ) 

        self.disp.on(tla.evtype.SysError,
            async (err) => {
                // TBD - not yet defined content, but no user message
                console.log(TRACEHDR + " on SysError ", err);
                fsm.error(err);
            }
        ) 

        self.disp.on(tla.evtype.Usermsg,
           async (event) => {
               // {evtype: "urm", status: <"ok" | "invalid">, evdata: {message: <msg string>, severity: <"warn" | "fail">}}
               // msg might contain newline ascii chars '\n'
                console.log(TRACEHDR + " on Usermsg ", event);
                fsm.usermsg(event);
           }
        )

        self.disp.on(tla.evtype.CommStatus,
           async (event) => {
                // {evtype: "cos", status: <"ok" | "invalid">, network: <"connected" | "disconnected">, firstconnect: <true | false>}
                console.log(TRACEHDR + " on CommStatus ", event);
                if (event.network && event.firstconnect)
                    fsm.commStatus(event.network, event.firstconnect);
           }
        )

        self.disp.on(tla.evtype.BrowserAppState,
           async (event) => {
                console.log(TRACEHDR + " on BrowserAppState ", event);
                fsm.browserAppState();
           }
        )

        self.disp.on(tla.evtype.SysopInfoEvent,
            async (event) => {
                // {evtype: "soe", status: <"ok" | "fail" | "invalid">, 
                // evdata: {isOngoing: <true | false>, isCompleted: <true | false>, isFailed: <true | false>
                // <failcode: <code>, rejected: <true | false> | ^ > }}
                console.log(TRACEHDR + " on SysopInfoEvent event", event);
                fsm.sysop(event);
            }
        )

        self.disp.on(tla.evtype.LocalServer,
            async (event) => {
                // {evtype: "slo", evdata: <free jsobject>}
                console.log(TRACEHDR + " on LocalServer event", event);
                jso = JSON.parse(event.evdata);
                fsm.localServer(jso);
            }
        )

        self.disp.on(tla.evtype.Rfid,
            async (event) => {
                // {evtype: "rfi", status: <"ok", | "rejected">, evdata: <fobid string>}
                console.log(TRACEHDR + " on Rfid event");
                fsm.identify(event);
            }
        )

        self.disp.on(tla.evtype.FpIdentify,
            async (event) => {
                // {evtype: "fpi", status: <"ok" | "rejected">, evdata: { < {globalid: <gid string>, tipix: <tipix num> | ^ > } }
                console.log(TRACEHDR + " on FpIdentify event");
                fsm.identify(event);
            }
        ) 

        self.disp.on(tla.evtype.FpEnrollCheck,
            async (event) => {
                // {evtype: "fec", status: <"ok" | "invalidinput" | "invalidscan" | "rejectedscan" | "aborted" | "failed">, 
                // evdata: { origin: <#>, seqno: <#>, {templid: <templid string>} | {reason: "code"} ]}
                console.log(TRACEHDR + " on FpEnrollCheck event ", event);
                fsm.fpEnroll(event);
            }
        )

        self.disp.on(tla.evtype.FpEnrollFeedback,
            async (event) => {
                // {evtype: "fef", status: <"ok" | "invalidinput" | "invalidscan" | "rejectedscan" | "aborted" | "failed">, 
                // evdata: { origin: <#>, seqno: <#>, phase: <1,2,3> }}
                console.log(TRACEHDR + " on FpEnrollFeedback event ", event);
                fsm.fpEnroll(event);
            }
        )

        self.disp.on(tla.evtype.FpEnrollCompleted,
           async (event) => {
               // {evtype: "fed", status: <"ok" | "invalidinput" | "invalidscan" | "rejectedscan" | "aborted" | "failed">, 
               // evdata: { origin: <#>, seqno: <#>, < templateid: <tid> | rc: <return code>,  message: <rc as text> > }}
                console.log(TRACEHDR + " on FpEnrollCompleted ", event);
                fsm.fpEnroll(event);
            }
        )

        self.disp.on(tla.evtype.FpData,
           async (event) => {
                // {evtype: "fed", status: < "ok" | "invalidinput" | "invalidscan" | "rejectedscan" | "aborted" | "failed" >, 
                // evdata: {origin: <#>, seqno: <#>, 
                // templates: [ {templateid: <tid>, blob: <data>}, ... ]
                // sha1: <sha1 hash>, templtype: "zktecho10.0", encoding: "base64"}}
                // < templates has array 1 to 10, sha1 is hash of all templates in array>
                console.log(TRACEHDR + " on FpData ", event);
                fsm.fpData(event);
           }
        )

        self.disp.on(tla.evtype.FpDeletedOne,
           async (event) => {
                // TBD
                console.log(TRACEHDR + " on FpDeletedOne ", event);
                fsm.fpDeleted(event);
           }
        )

        self.disp.on(tla.evtype.FpExport,
           async (event) => {
                // TBD
                console.log(TRACEHDR + " on FpExport ", event);
                fsm.fpExport(event);
           }
        )

        self.disp.on(tla.evtype.FpOpCompleted,
            async (event) => {
                // [Amended 0.4]
                // {evtype: "foc", status: <"ok" | "fail">
                // evdata: {origin: <#>, seqno: <#>
                // operation: < "reset" | "status" | "param" | "import" | "export" | "unknown" >,  status: <"ok" | "fail" | "enabled" | "disabled" | "invalid" >  }}
                console.log(TRACEHDR + " on FpOpCompleted ", event);
                fsm.fpOpCompleted(event);
           }
        )

        self.disp.on(tla.evtype.FpImport,
           async (event) => {
                // [Amended 0.6]
                // {evtype: "fit", status: <"ok" | "fail" >
                // evdata: {origin: <#>, seqno: <#>, import: < "all" | "partial" >, delete: <"all" | "noop" | "partial">,
                // return: [{ix: <#>, success: <true | false>, templateid: "", rc: <#>, message: ""} ]}
                //}
                console.log(TRACEHDR + " on FpImport ", event);
                fsm.fpImportTemplate(event);
           }
        )

        self.disp.on(tla.evtype.FpAmendTemplates,
            async (event) => {
                // [Amended 0.6]
                // {evtype: "fa5", status: <"ok" | "fail" >
                // evdata: {origin: <#>, seqno: <#>, import: < "all" | "partial" >, delete: <"all" | "noop" | "partial">,
                // return: [{ix: <#>, success: <true | false>, templateid: "", rc: <#>, message: ""}, ... ]}
                //}
                console.log(TRACEHDR + " on FpAmendTemplates ", event);
                fsm.fpAmendTemplates(event);
           }
        )

        self.disp.on(tla.evtype.CamExposure,
           async (event) => {
                // {evtype: "cae", status: < "ok" | "fail" >
                // evdata: {origin: <#>, seqno: <#>, type: < "jpeg" | "other" >
                // blob: <data>, encoding: "base64"}}
                console.log(TRACEHDR + " on CamExposure ", event);
                fsm.camExposure(event);
           }
        )

        self.bkend.on(tla.netevent.Pumpevent,
            async (evlist) => {
                // Backend event handler for Pump response, returning the event array from GET /event/list
                console.log(TRACEHDR + " Pump event list", evlist)
                fsm.pumpEvents(evlist);
            }
        )
    } // eo func
} // eo self

module.exports = () => {
    return self;
}