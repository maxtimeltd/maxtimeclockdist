//-----
// nodejs app script - nodejs js module for database access
//-----

var TRACEHDR = "TRACE-MYDB "

//const sqlite3  = require('sqlite3');

mydb = {
    handler: null,
    tdb: null,
/*
    setupdb: (wrap) =>  {
        let dbfile = process.env.TLA_DBFILEPATH;

        mydb.tdb = wrap.tdb;

        mydb.handler = new sqlite3.Database(dbfile, sqlite3.OPEN_READWRITE, (err) => {console.log(err); });
       
        console.log("Local db opened");
    },
*/
    setupdb: (wrap) =>  {
        // arg supposed to be from tla4nodejs wrap
        mydb.tdb = wrap.tdb;
        mydb.handler = mydb.tdb.open(
            (err) => {
                if(err)
                    console.log("Fail database open ", err);
            }
        );
        console.log("Done setup of db", mydb.handler);
    },

    queryschemaver: (cb) => {
        var h = mydb.handler;
        h.get(
            `SELECT ver FROM schema_ver order by stamp desc limit 1`,
            (err, row) => {
                console.log(TRACEHDR + "queryschemaver", err, row);
                if(err)
                    cb("Fail reading db");
                if(row && row.ver)
                    cb(row.ver);
            }
        );
    },

    logtodb: (msg) => {
        mydb.handler.run(
            `INSERT into log (timestamp, event) values (time(), ?)`, [msg],
            (err) => {
                if (err)
                    console.error(TRACEHDR + "Fail, insert into log", err);
            }
        )

    },

    getschemaver: async (cb) => {
        var h = mydb.handler;
        h.get(
            `SELECT ver FROM schema_ver ORDER BY stamp DESC LIMIT 1`,
            (err, row) => {
                if (err)
                    cb("Fail reading db");
                if (row && row.ver)
                    cb(row.ver);
            }
        );
    },

    getversion: async (cb) => {
        var h = mydb.handler;
        h.get(
            `SELECT ver, stamp FROM mtversion ORDER BY stamp DESC LIMIT 1`,
            (err, row) => {
                if (err)
                    cb(null);
                else
                    cb(row);
            }
        );
    },

    getusers: async (cb) => {
        var h = mydb.handler;
        h.all(
            `SELECT id, code, name FROM mtuser`, 
            (err, data) => {
                if (err) {
                    cb({result: 1, data: null});
                } else if (!data) {
                    cb({result: 1, data: null});
                } else  {
                    cb({result: 0, data: data});
                }
            }
        );
    },

    getuserbyid: async (id, cb) => {
        var h = mydb.handler;
        h.get(
            `SELECT id, code, name FROM mtuser WHERE id = ? `, [id], 
            (err, row) => {
                if (err) {
                    cb({result: 1, data: err});
                } else if (!row) {
                    cb({result: 1, data: null});
                } else {
                    cb({result: 0, data: row});
                }
            }
        );
    },

    getuserbycode: async (code, cb) => {
        var h = mydb.handler;
        h.get(
            `SELECT id, code, name FROM mtuser WHERE code = ?`, [code], 
            (err, row) => {
                if (err) {
                    cb({result: 1, data: err});
                } else if (!row) {
                    cb({result: 1, data: null});
                } else {
                    cb({result: 0, data: row});
                }
            }
        );
    },

    getuserbyrfid: async (rfid, cb) => {
        var h = mydb.handler;
        h.get(
            `SELECT id, code, name FROM mtuser WHERE rfid = ?`, [rfid], 
            (err, row) => {
                if (err) {
                    cb({result: 1, data: null});
                } else if (!row) {
                    cb({result: 1, data: null});
                } else {
                    cb({result: 0, data: row});
                }
            }
        );
    },

    getuserbypin: async (id, pin, cb) => {
        var h = mydb.handler;
        h.get(
            `SELECT id, code, name FROM mtuser WHERE (id = ?) AND (pin = ?)`, [id, pin], 
            (err, row) => {
                if (err) {
                    cb({result: 1, data: err});
                } else if (!row) {
                    cb({result: 1, data: null});
                } else {
                    cb({result: 0, data: row});
                }
            }
        );
    },

    adduser: async (user, cb) => {
        var h = mydb.handler;
        h.run(
            `INSERT INTO mtuser (id, created, code, name, finger, rfid, pin) VALUES (?, ?, ?, ?, ?, ?, ?)`, [user.id, user.created, user.code, user.name, user.finger, user.rfid, user.pin], 
            function (err) {
                if (err) {
                    cb({result: 1, data: err});
                } else {
                    cb({result: 0, data: {id: this.lastID}});
                }
            }
        );
    },

    deleteuserbyid: async (id, cb) => {
        var h = mydb.handler;
        h.run(
            `DELETE FROM mtuser WHERE id = ?`, [id], 
            (err) => {
                if (err) {
                    cb({result: 1, data: err});
                } else {
                    cb({result: 0, data: {id: id}});
                }
            }
        );
    },

    deleteuserbycode: async (code, cb) => {
        var h = mydb.handler;
        h.run(
            `DELETE FROM mtuser WHERE code = ?`, [code], 
            (err) => {
                if (err) {
                    cb({result: 1, data: err});
                } else {
                    cb({result: 0, data: null});
                }
            }
        );
    },

    addpunch: async (user, punch, method, type, photo, cb) => {
        var h = mydb.handler;
        h.run(
            `INSERT INTO mtpunch (user, punch, method, type, photo, processed) VALUES (?, ?, ?, ?, ?, 0)`, [user, punch, method, type, photo], 
            function (err) {
                if (err) {
                    cb({result: 1, data: err});
                } else {
                    cb({result: 0, data: {id: this.lastID}});
                }
            }
        );
    },

    getpunches: async (limit, cb) => {
        var h = mydb.handler;
        h.all(
            `SELECT p.id, p.user, p.punch, p.method, p.type, p.processed, u.name, u.code FROM mtpunch p INNER JOIN mtuser u ON u.id = p.user ORDER BY p.punch DESC LIMIT 50`,
            (err, data) => {
                if (err) {
                    cb({result: 1, data: null});
                } else if (!data) {
                    cb({result: 1, data: null});
                } else  {
                    cb({result: 0, data: data});
                }
            }
        );
    },

    getunprocessedpunches: async (limit, cb) => {
        var h = mydb.handler;
        h.all(
            `SELECT id, user, punch, method, type FROM mtpunch WHERE processed = 0 ORDER BY punch ASC LIMIT ?`, [limit], 
            (err, data) => {
                if (err) {
                    cb({result: 1, data: null});
                } else if (!data) {
                    cb({result: 1, data: null});
                } else  {
                    cb({result: 0, data: data});
                }
            }
        );
    },

    setpunchprocessed: async (id, cb) => {
        var h = mydb.handler;
        h.run(
            `UPDATE mtpunch SET processed=1 WHERE id = ?`, [id], 
            (err) => {
                if (err) {
                    cb({result: 1, data: err});
                } else {
                    cb({result: 0, data: null});
                }
            }
        );
    },

    setpunchesprocessed: async (ids, cb) => {
        var h = mydb.handler;
        h.run(
            `UPDATE mtpunch SET processed=1 WHERE id IN (?)`, [ids], 
            (err) => {
                if (err) {
                    cb({result: 1, data: err});
                } else {
                    cb({result: 0, data: null});
                }
            }
        );
    },

    addfailpunch: async (punch, method, photo, cb) => {
        var h = mydb.handler;
        h.run(
            `INSERT INTO mtpunchfail (punch, method, photo, processed) VALUES (?, ?, ?, ?, 0)`, [punch, method, photo], 
            function (err) {
                if (err) {
                    cb({result: 1, data: err});
                } else {
                    cb({result: 0, data: {id: this.lastID}});
                }
            }
        );
    },

    getfailpunches: async (limit, cb) => {
        var h = mydb.handler;
        h.all(
            `SELECT id, punch, method, processed FROM mtpunchfail ORDER BY punch DESC LIMIT 50`,
            (err, data) => {
                if (err) {
                    cb({result: 1, data: null});
                } else if (!data) {
                    cb({result: 1, data: null});
                } else  {
                    cb({result: 0, data: data});
                }
            }
        );
    },

    getunprocessedfailpunches: async (limit, cb) => {
        var h = mydb.handler;
        h.all(
            `SELECT id, punch, method FROM mtpunchfail WHERE processed = 0 ORDER BY punch DESC LIMIT ?`, [limit], 
            (err, data) => {
                if (err) {
                    cb({result: 1, data: null});
                } else if (!data) {
                    cb({result: 1, data: null});
                } else  {
                    cb({result: 0, data: data});
                }
            }
        );
    },

    setfailpunchprocessed: async (id, cb) => {
        var h = mydb.handler;
        h.run(
            `UPDATE mtpunchfail SET processed=1 WHERE id = ?`, [id], 
            (err) => {
                if (err) {
                    cb({result: 1, data: err});
                } else {
                    cb({result: 0, data: null});
                }
            }
        );
    },

    closedb: () => {
        if(mydb.tdb)
            mydb.tdb.close();
        mydb.handler = null;
    }
};

module.exports = (wrap) => {
    return mydb;
}