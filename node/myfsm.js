//-----
// nodejs app script - nodejs js module for state machine
//
//-----

var TRACEHDR = "TRACE-MYFM "


// Get us a finit state machine with the app logic
//const mydb = require('./mydb')();

const tre = require('ts-remote');

const con = require ('ts-config');

// Get us a fingerprintmodule api object
//const tfp = require('ts-fpm');

// Get us a file system object
//const fs = require('fs');
console.log("Require done");


var g = {
    counter: 0,
    specialId: 17,
    attOnExposure: false
}

var progressLog = [];

//----- backend event pump ------------------
var mtTimeout = null;

var state = null;

var DEBUG_ON = false;

const ENROL_FINGERPRINT_STAGE_MAX = 3;

const TIMEOUT_ENROL = 1;
const TIMEOUT_RESTART = 5;
const TIMEOUT_REDEPLOY = 1;
const TIMEOUT_PAUSE = 500;

const EVENT_USER = "USER";
const EVENT_DELETEUSER = "DELETEUSER";
const EVENT_RESTART = "RESTART";
const EVENT_REDEPLOY = "REDEPLOY";
const EVENT_GETCONFIG = "GETCONFIG";
const EVENT_UICONFIG = "UICONFIG";

const CMD_DEVICE_SERIAL_NO = "DEVICE_SERIAL_NO";
const CMD_DEVICE_INFORMATION = "DEVICE_INFORMATION";
const CMD_DEVICE_RESTART = "DEVICE_RESTART";
const CMD_EMPLOYEES_LIST = "EMPLOYEES_LIST";
const CMD_PUNCHES = "VIEW_PUNCHES";
const CMD_PUNCH_IN = "PUNCH_IN";
const CMD_PUNCH_OUT = "PUNCH_OUT";
const CMD_BREAK_START = "BREAK_START";
const CMD_BREAK_END = "BREAK_END";
const CMD_PUNCH_CANCEL = "PUNCH_CANCEL";
const CMD_PIN_LOGIN = "PIN_LOGIN";
const CMD_INPUT_RECEIVED = "INPUT_RECEIVED";
const CMD_AUTH = "AUTH";
const CMD_ENROL = "ENROL";
const CMD_ENROL_FINGERPRINT_CONFIRM = "ENROL_FINGERPRINT_CONFIRM";
const CMD_ENROL_RFID_CONFIRM = "ENROL_RFID_CONFIRM";
const CMD_ENROL_PIN_CONFIRM = "ENROL_PIN_CONFIRM";
const CMD_ENROL_CANCEL = "ENROL_CANCEL";
const CMD_ENROL_FINGERPRINT = "ENROL_FINGERPRINT";
const CMD_ENROL_FINGERPRINT_1 = "ENROL_FINGERPRINT_1";
const CMD_ENROL_FINGERPRINT_2 = "ENROL_FINGERPRINT_2";
const CMD_ENROL_FINGERPRINT_3 = "ENROL_FINGERPRINT_3";
const CMD_ENROL_RFID = "ENROL_RFID";
const CMD_ENROL_PIN = "ENROL_PIN";
const CMD_ENROL_COMPLETE = "ENROL_COMPLETE";
const CMD_UICONFIG = "CONFIG_UPDATE";
const CMD_BACKUP = "BACKUP";
const CMD_RESTORE = "RESTORE";
const CMD_PING = "PING";

const CMD_DEBUG = "DEBUG";

const CMD_ENROL_FINGERPRINT_STAGE = [
    CMD_ENROL_FINGERPRINT_1,
    CMD_ENROL_FINGERPRINT_2,
    CMD_ENROL_FINGERPRINT_3
];

const STATE_PUNCH_IN = "PUNCH_IN";
const STATE_PUNCH_OUT = "PUNCH_OUT";
const STATE_BREAK_START = "BREAK_START";
const STATE_BREAK_END = "BREAK_END";
const STATE_ENROL = "ENROL";
const STATE_AUTH = "AUTH";

const PUNCH_STATES = [
    STATE_PUNCH_IN, STATE_PUNCH_OUT, STATE_BREAK_START, STATE_BREAK_END
];

const ENROLSTATE_FINGER = "FINGER";
const ENROLSTATE_RFID = "RFID";
const ENROLSTATE_PIN = "PIN";
const ENROLSTATE_PHOTO = "PHOTO";
const ENROLSTATE_ENROL = "ENROL";

var enrolMethods = {
    fingerprint: 0,
    rfid: false,
    pin: 0,
    photo: false,
};
var enrolState = null;
var enrolId = null;
var enrolName = null;
var enrolCode = null;
var enrolRfid = null;
var enrolPin = null;
var enrolFinger = [];
var enrolFingerCount = 0;
var enrolFingerStage = 0;
var expPhoto = null;

var amendId = null;
var amendName = null;
var amendCode = null;
var amendRfid = null;
var amendPin = null;
var amendFinger = [];

function resetState() {
    state = null;
    enrolState = null;
    enrolId = null;
    enrolName = null;
    enrolCode = null;
    enrolMethods = {
        fingerprint: 0,
        rfid: false,
        pin: 0,
        photo: false,
    };
    enrolRfid = null;
    enrolPin = null;
    enrolFinger = [];
    enrolFingerCount = 0;
    enrolFingerStage = 0;
    expPhoto = null;
}

function resetAmendState() {
    amendId = null;
    amendName = null;
    amendCode = null;
    amendRfid = null;
    amendPin = null;
    amendFinger = 0;
}

function getCurrentDateTimeString() {
    const date = new Date();
    return date.getFullYear() + '-' +
        (date.getMonth() + 1).toString().padStart(2, '0') + '-' +
        date.getDate().toString().padStart(2, '0') + ' ' +
        date.getHours().toString().padStart(2, '0') + ':' +
        date.getMinutes().toString().padStart(2, '0') + ':' +
        date.getSeconds().toString().padStart(2, '0');
}

function formatMTVer(ver) {
    return Math.floor(ver/1000000).toString() 
        + '.' + (Math.floor(ver/1000) - (Math.floor(ver/1000000) * 1000)).toString()
        + '.' + (ver - (Math.floor(ver/1000) * 1000)).toString();
}

function toBrowser(cmd, result, data) {
    disp.toBrowser(
        {
            cmd: cmd, 
            result: result,
            data: data
        }
    );
}

function authFail(message) {
    toBrowser("AUTH", 1, {message: message});
    resetState();
}

function enrolFail(message) {
    toBrowser("ENROL", 1, {message: message});
    resetState();
}

async function punchFail(message) {

    toBrowser(state, 1, null);

    let now = getCurrentDateTimeString();

    await mydb.addfailpunch(now, null, null, 
        async (res) => {
            
            postAttendanceFailed(res.data.id, now);

        }).catch(error => { 

            postAttendanceFailed(null, now);
        
        });                

}

function fail(cmd, message) {
    toBrowser(cmd, 1, {message: message});
    resetState();
}

function debug(cmd, result, data) {
    if (DEBUG_ON === true) {
        toBrowser(cmd.toUpperCase(), result, data);
    }    
}

///////////////////////////////////////

//    COMMS WITH SERVER

///////////////////////////////////////

// Post an attendance for a userid
async function postAttendance(id, data) {
    console.log(TRACEHDR + "  Attendance", data.id.toString());

    debug("POSTATTENDANCE", 0, {id: id, data: data});

    if (!data)
        return;
    
    endp0 = "/attendance/" + data.id.toString();
    bkend.requestCb(
        {method: bkend.netmethod.Post, endpoint: endp0},
        {punch: data.punch, method: data.method, photo: {format: "jpg", encoding: "base64", data: expPhoto}},
        function (err, response, payload) {
            if (err) {

                debug(state, 0, {error: err});
//                toBrowser(state, 1, {id: data.id, code: data.code, name: data.name, datetime: data.punch});
                resetState();

            } else {
                debug(state, 0, {response: response, payload: payload});

                mydb.setpunchprocessed(id,
                    (row) => {

                        debug("PUNCHPROCESSED", 0, row);

                        if ((row !== null) && (row.result === 0)) {

                            debug("PUNCHPROCESSED", 0, {message: 'ok'});

                            resetState();

                        }  else {
                            resetState();
                        }    

                    }).catch(error => { 
                        resetState();
                    });    
            }
        });

}

// Post an attendance failure  for a userid
async function postAttendanceFailed(id, datetime) {
    console.log(TRACEHDR + "  Attendance Failed");
    
    endp0 = "/attendance/failed";
    bkend.requestCb(
        {method: bkend.netmethod.Post, endpoint: endp0},
        {punch: datetime, photo: {format: "jpg", encoding: "base64", data: expPhoto} },
        (err, response, payload) => {
            if (err) {

                debug(state, 0, {error: err});
//                toBrowser(state, 1, {id: data.id, code: data.code, name: data.name, datetime: data.punch});
                resetState();

            } else {

                debug(state, 0, {response: response, payload: payload});

                if (id !== null) {

                    mydb.setfailpunchprocessed(id,
                        (row) => {

                            debug("PUNCHPROCESSED", 0, row);

                            if ((row !== null) && (row.result === 0)) {

                                debug("PUNCHPROCESSED", 0, {message: 'ok'});

                                resetState();

                            }  else {
                                resetState();
                            }    

                        }).catch(error => { 
                            resetState();
                        });  

                } else {
                    resetState();
                }  
            }
                  
        });

}

// Post enrolment for a userid
async function postEnrol(dateTime) {
    console.log(TRACEHDR + " Enrol", enrolId.toString());
    
    endp0 = "/user/" + enrolId.toString();
    bkend.requestCb(
        {method: bkend.netmethod.Post, endpoint: endp0},
        {datetime: dateTime, rfid: enrolRfid, pin: enrolPin, finger: enrolFinger, photo:{format: "jpg", encoding: "base64", data: expPhoto}},
        (err, response, payload) => {
            if (err) {
                toBrowser(CMD_ENROL, 1, {id: enrolId, code: enrolCode, name: enrolName, datetime: dateTime});
            } else {
                toBrowser(CMD_ENROL, (response.statusCode == 200 ? 0 : 1), {id: enrolId, code: enrolCode, name: enrolName, datetime: dateTime});
            }

            resetState();
        }
    );
}

// Mark event as processed
async function markEvent(evid) {
    console.log(TRACEHDR + " Mark event", evid);

    if (!evid) {
        console.log(TRACEHDR + " Mark event, null", evid)
        return;
    }

    bkend.requestCb(
        {method: bkend.netmethod.Patch, endpoint: "/event/processed/" + evid.toString()},
        {},
        (err, response, payload) => {
            if (err) {
                console.log(TRACEHDR + " Mark event, error response ", err)
                return
            }
            if (response.rc == 0)
                console.log(TRACEHDR + " Mark event/2 ok")
            else
                console.log(TRACEHDR + " Mark event/2 evid fishy, resp, pl", evid, response, payload)
        }
    );
}

///////////////////////////////////////

//    HANDLE SERVER EVENTS

///////////////////////////////////////

async function handleEventUser(evid, obj) {

    debug("USER", 0, obj);

    markEvent(evid);

    amendId = obj.id;
    amendName = obj.name;
    amendCode = obj.code;
    amendRfid = obj.rfid;
    amendPin = obj.pin;
    amendFinger = obj.finger.length;
    
    //create a matrix of templateid and blobs
    let tid = obj.id.toString() + "#0";
    let tmplts = [];
    for (let n = 0; n < obj.finger.length; n++) {        
        //let tid = g.latestUserId + "#" + (n+1);              
        tmplts[n] = { 
            templateid: tid,
            blob: obj.finger[n].template
        }
    }
    
    // Amend the array of templates for a user to fingerprint-module
    await disp.sendEvent({
        evtype: disp.evtype.FpCmdAmendTemplates,
        evdata: {
            origin: 1000,
            seqno: 1,
            templateid: tid,
            templates: tmplts,
            sha1: 'G5e0S4o89aaQ3ujGLVFh8/bvmpQ=',
            templtype: 'zktecho10.0',
            encoding: 'base64'
        }
    });

    debug("USER", 0, {message: "FpCmdAmendTemplates"});

}
/*
async function handleEventUser2(evid, obj) {

    debug("USER", 0, obj);

    markEvent(evid);

    for (let n = 0; n < obj.finger.length; n++) {

        // write each template to a separate file
        let base64String = obj.finger[n].template;
        let rawData = Buffer.from(base64String, "base64");
//        let rawData = Buffer.from(base64String);
//        debug("USER", 0, {n: n, base64: base64String, raw: rawData});
        let filePath = "/persist/tla/tmp/tmpl-userid-" + obj.id.toString() + "-tix-" + n.toString();
        try {
            // fs is a filesystem api object, required in start section of this file
//            fs.writeFileSync(filePath, base64String, "utf8");
            fs.writeFileSync(filePath, rawData, "utf8"); 
            debug("USER", 0, {message: "Data has been written to file"});
            console.log("Data has been written to file");
        } catch (error) {
            console.error("An error occurred: ", error);
            debug("USER", 1, {message: error});
        }
        
    }
    
    // tfp is a fingerprintmodule api object, required in start section of this file
    // All fetched templates for users are imported. Temp files are deleted from the tmp-folder after import.
    tfp.cmdFpImportOneTemplateFile("importall " + obj.id.toString(), obj.finger.length);

    debug("USER", 0, {message: "Imported"});

    let now = getCurrentDateTimeString();
    await mydb.adduser({id: obj.id, created: now, code: obj.code, name: obj.name, finger: obj.finger.length, rfid: obj.rfid, pin: obj.pin},
        async (row) => {
            debug("USER", 0, row);
            if ((row !== null) && (row.result === 0)) {
                debug("USER", 0, {message: 'ok'});
            }  else {
                debug("USER", 1, {row: row});
            }                 
        }).catch(error => { 
            debug("USER", 0, {message: error});
        });    

}
*/
async function handleEventUserDelete(evid, obj) {

    debug("USERDELETE", 0, obj);

    markEvent(evid); 

    await mydb.deleteuserbyid(obj.id,
        async (row) => {
            debug("USERDELETE", 0, row);
            if ((row !== null) && (row.result === 0)) {
                let tid = obj.id.toString() + "#0";
                await disp.sendEvent(
                    {evtype: disp.evtype.FpCmdDeleteOne, evdata:{templateid: tid}}
                );

                debug("USERDELETE", 0, {message: 'ok'});

            }  else {
                debug("USERDELETE", 1, {row: row});
            }   

        }).catch(error => { 
            debug("USERDELETE", 1, {message: error});
        });    

}

async function handleEventUserDelete2(evid, obj) {

    debug("USERDELETE", 0, obj);

    markEvent(evid); 

    let id = null

    await mydb.getuserbycode(obj.code,
        async (row) => {
            debug("USERDELETE", 0, row);
            
            if ((row !== null) && (row.result === 0)) {

                clearTimeout(mtTimeout); 
    
                mtTimeout = setTimeout(
                    async () => {
   
                        await mydb.deleteuserbyid(row.data.id,
                            async (row) => {
                                debug("USERDELETE", 0, row);
                                if ((row !== null) && (row.result === 0)) {
                                    let tid = row.data.id.toString() + "#0";
                                    await disp.sendEvent(
                                        {evtype: disp.evtype.FpCmdDeleteOne, evdata:{templateid: tid}}
                                    );

                                    debug("USERDELETE", 0, {message: 'ok'});

                                }  else {

                                }                 
                            }).catch(error => { 

                            });    

                        clearTimeout(mtTimeout); 
    
                }, Math.abs(TIMEOUT_PAUSE));
            
            }  else {

            }                 
        }).catch(error => { 

        });                

}

async function handleEventGetConfig(evid, obj) {

    debug("GETCONFIG", 0, obj);

    tre.cmdGetDeviceConfig()
        .then(result => { 
            toBrowser(CMD_DEVICE_SERIAL_NO, 0, result);
        })
        .catch(error => { 
            toBrowser(CMD_DEVICE_SERIAL_NO, 1, null);
        });

//    await disp.sendEvent(
//        {evtype: disp.evtype.FpCmdConfig}
//    );

    markEvent(evid);
}

async function handleEventRestart(evid, obj) {
    
    debug("RESTART", 0, obj);

    markEvent(evid);

    toBrowser(CMD_DEVICE_RESTART, 0, {timeout: TIMEOUT_RESTART});

    clearTimeout(mtTimeout); 
    
    mtTimeout = setTimeout(
        async () => {
            disp.sysopReboot();
        }, Math.abs(TIMEOUT_RESTART) * 1000);

}

async function handleEventRedeploy(evid, obj) {
    
    debug("REDEPLOY", 0, obj);

    markEvent(evid);

    toBrowser(CMD_DEVICE_RESTART, 0, {timeout: TIMEOUT_RESTART});

    clearTimeout(mtTimeout); 
    
    mtTimeout = setTimeout(
        async () => {
            disp.sysopRedeployTlaApp();
        },  Math.abs(TIMEOUT_REDEPLOY) * 1000);

}

async function handleEventUIConfig(evid, obj) {
    
    debug("UICONFIG", 0, obj);

    markEvent(evid);

    toBrowser(CMD_UICONFIG, 0, null);

}

// Process single event from event pump
async function handlePumpEvent(ev) {
    lbl = TRACEHDR + " handlePumpEvent, ";

    debug("PUMP_EVENT", 0, ev);

    console.log(lbl, ev)
    if(!ev) {
        console.log(lbl + "null event")
        return;
    }

    endp0 = "/event/" + ev.evid.toString()
    bkend.requestCb(
        {method: bkend.netmethod.Get, endpoint: endp0},
        {},
        (err, response, obj) => {
            
            debug("EVENT", 0, obj);

            // check error and completeness
            if (err) {
                console.log(lbl + "error response ", err)
                return
            }
            if (response.rc != 0) {
                console.log(lbl + "bkend event rejected, no payload or bad rc ", obj, response)
                return
            }
            if (!obj.evtype) {
                console.log(lbl + "missing event", obj)
                return;
            }

            // check events
            switch (obj.evtype) {

                case EVENT_USER:
                    handleEventUser(obj.evid, obj.payload);
                break;

                case EVENT_DELETEUSER:
                    handleEventUserDelete(obj.evid, obj.payload);
                break;

                case EVENT_GETCONFIG:
                    handleEventGetConfig(obj.evid, obj.payload);
                break;

                case EVENT_RESTART:
                    handleEventRestart(obj.evid, obj.payload);
                break;

                case EVENT_REDEPLOY:
                    handleEventRedeploy(obj.evid, obj.payload);
                break;

                case EVENT_UICONFIG:
                    handleEventUIConfig(obj.evid, obj.payload);
                break;

                default:
                    console.log(lbl + " unhandled event ", obj)
            }
        }
    );
}

///////////////////////////////////////

//    ENROLMENT

///////////////////////////////////////

async function enrolNextStage() {
    clearTimeout(mtTimeout); 
    
    mtTimeout = setTimeout(
        async () => {

            if (enrolFingerCount < enrolMethods.fingerprint) {

                enrolState = null;
                enrolFingerCount++;
                enrolFingerStage = 1;
        
                toBrowser(CMD_ENROL_FINGERPRINT, 0, {index: enrolFingerCount});

            } else if ((enrolMethods.rfid === true) && (enrolRfid == null)) {

                enrolState = null;

                toBrowser(CMD_ENROL_RFID, 0, null);
        
            } else if ((enrolMethods.pin > 0) && (enrolPin == null)) {

                enrolState = null;

                toBrowser(CMD_ENROL_PIN, 0, null);
        
            } else if ((enrolMethods.photo === true) && (expPhoto === null)) {

                enrolState = null;

                toBrowser(CMD_ENROL_PHOTO, 0, null);
        
            } else {

                // no more so save it

                enrolState = ENROLSTATE_ENROL;

                toBrowser(CMD_ENROL_COMPLETE, 0, null);

                var now = getCurrentDateTimeString();

                await mydb.adduser({
                        id: enrolId, 
                        created: now, 
                        code: enrolCode, 
                        name: enrolName, 
                        finger: enrolFinger.length, 
                        rfid: enrolRfid, 
                        pin: enrolPin
                    }, async (row) => {
                        if ((row !== null) && (row.result === 0)) {

                            await postEnrol(now);

                        }  else {

                            enrolFail('Error add user to db');
 
                        }                 
                    
                    }

                ).catch(error => { 

                    toBrowser(CMD_ENROL, 1, {id: enrolId, code: enrolCode, name: enrolName, datetime: now});
    
                    resetState();
            
                });   
            }
        }, Math.abs(TIMEOUT_ENROL) * 100);
}

async function processEnrol(ev) {

    debug("ENROLPROGRESS", 0, ev);

    if (ev.status == 'ok') {

        if (ev.evtype == tla.evtype.FpEnrollCheck) {

        } else if (ev.evtype == tla.evtype.FpEnrollFeedback) {

            if ((state === STATE_ENROL) && (enrolState === ENROLSTATE_FINGER) && (enrolFingerStage < ENROL_FINGERPRINT_STAGE_MAX)) {

                toBrowser(CMD_ENROL_FINGERPRINT_STAGE[enrolFingerStage], 0, null);

                enrolFingerStage++;

            }

        } else if (ev.evtype == tla.evtype.FpEnrollCompleted) {

            await disp.sendEvent(
                {evtype: disp.evtype.FpCmdNextStepEnroll, evdata:{nextstep: "fpdata", templateid: enrolId.toString() + '#' + (enrolFingerCount - 1).toString()}}
            );

        }

    } else {

        enrolFail("Error stage failed " + ev.status);

    } 
}

async function processEnrolFingerData(ev) {

    debug("ENROLDATA", 0, ev);

    if (ev.status == 'ok') {

        let tmp = Buffer.from(ev.evdata.templates[ev.evdata.templates.length - 1].blob, 'base64').toString('base64');

        // trunc the string to exactly 2220 chars
        if (tmp.length > 2219) {
            tmp = tmp.substring(0, 2219);
        } else {
            while (tmp.length < 2219) {
                tmp += "=";
            }
        }

//        let tmp = ev.evdata.templates[ev.evdata.templates.length - 1].blob;

        enrolFinger.push({
            index: enrolFinger.length + 1,
            encoding: "base64",
            template: tmp
        });

        enrolNextStage();

    } else {
        enrolFail('Error fingerprint data');
    }
}

///////////////////////////////////////

//    IDENTIFY

///////////////////////////////////////

async function processIdentify(ev) {

    debug("IDENTIFY", 0, ev);

    if (PUNCH_STATES.includes(state)) {

        toBrowser(CMD_INPUT_RECEIVED, 0, null);

        let now = getCurrentDateTimeString();

        if (ev.evtype === tla.evtype.Rfid) {
        
            if (ev.status == 'ok') {

                await mydb.getuserbyrfid(ev.evdata,
                    async (row) => {

                        if ((row !== null) && (row.result === 0)) {

                            await mydb.addpunch(row.data.id, now, 'RFID', state, null, 
                                async (res) => {
                                    if ((res !== null) && (res.result === 0)) {
                                                
                                        debug(state, 0, {response: res});

                                        toBrowser(state, 0, {id: row.data.id, code: row.data.code, name: row.data.name, datetime: now});

                                        postAttendance(res.data.id, {
                                            id: row.data.id,
                                            punch: now,
                                            method: 'RFID'
                                        });

                                    }  else {

                                        punchFail("Error adding punch");
                                    
                                    }   

                                }).catch(error => { 

                                    punchFail('Exception adding punch');
                                
                                });    

                        }  else {

                            punchFail("Invalid user");
                           
                        }   

                    }).catch(error => { 

                        punchFail("Exception identifying user");

                    });   

            }  else {

                punchFail('Invalid tag');

            }

        } else if (ev.evtype == tla.evtype.FpIdentify) {

            if (ev.status == 'ok') {

                await mydb.getuserbyid(ev.evdata.globalid,
                    async (row) => {
                        
                        debug(state, 0, {row: row});

                        if ((row !== null) && (row.result === 0)) {

                            await mydb.addpunch(row.data.id, now, 'FINGERPRINT', state, null, 
                                async (res) => {
                                    if ((res !== null) && (res.result === 0)) {
                                                 
                                        debug(state, 0, {response: res});

                                        toBrowser(state, 0, {id: row.data.id, code: row.data.code, name: row.data.name, datetime: now});

                                        await postAttendance(res.data.id, {
                                            id: row.data.id,
                                            punch: now,
                                            method: 'FINGERPRINT'
                                        });

                                    }  else {

                                        punchFail("Error adding punch");
                                    
                                    }   

                                }).catch(error => { 

                                    punchFail('Exception adding punch');
                                
                                });    

                        }  else {

                            punchFail("Invalid user");
                           
                        }   

                    }).catch(error => { 

                        punchFail("Exception identifying user");

                    });   

            }  else {

                punchFail('Invalid fingerprint');

            }

        }

    } else if ((state == STATE_ENROL) && (enrolState == ENROLSTATE_RFID)) {

        if (ev.status == 'ok') {
    
            if (ev.evtype === tla.evtype.Rfid) {
        
                await mydb.getuserbyrfid(ev.evdata,
                    async (row) => {
                        if (row == null) { 
                            // problem
                            enrolFail("Error checking users");
                        } else if (row.result == 0) {
                            
                            // problem - already exists                    
                            enrolFail("User already exists");

                        }  else {

                            enrolRfid = ev.evdata;
                            enrolNextStage();

                        }                                  
                    }

                ).catch(error => { 
                    // problem
                    enrolFail("Error checking users " + error.toString());
                });   

            }  else {
                // problem
                enrolFail("Error incorrect event " + ev.evtype);
            }

        }  else {
            // problem
            enrolFail("Error event status " + ev.status);
        }

    } else if (state == STATE_AUTH) {

        if (ev.status == 'ok') {
    
            if (ev.evtype === tla.evtype.Rfid) {
        
                await mydb.getuserbyrfid(ev.evdata,
                    async (row) => {
                        if ((row !== null) && (row.result === 0)) {

                            toBrowser(CMD_AUTH, 0, {
                                        id: row.data.id,
                                        code: row.data.code,
                                        name: row.data.name,
                                        method: 'RFID'
                                    });
                        
                            resetState();

                        }  else {
                            authFail("Error user does not exist");
                        }                     
                    }

                ).catch(error => { 
                    // problem
                    authFail("Error checking users " + error.toString());
                });   

            } else if (ev.evtype === tla.evtype.FpIdentify) {
        
                await mydb.getuserbyid(ev.evdata.globalid,
                    async (row) => {
                        if ((row !== null) && (row.result === 0)) {

                            toBrowser(CMD_AUTH, 0, {
                                        id: row.data.id,
                                        code: row.data.code,
                                        name: row.data.name,
                                        method: 'FINGERPRINT'
                                    });
                        
                            resetState();
 
                        }  else {
                            authFail("Error user does not exist");
                        }                   
                    }

                ).catch(error => { 
                    // problem
                    authFail("Error checking users " + error.toString());
                });   

            }  else {
                // problem
                authFail("Error incorrect event " + ev.evtype);
            }
     
        }  else {
            // problem
            authFail("Error event status " + ev.status);
        }

    }

}

async function templateImported(ev) {
}

async function templatesAmended(ev) {

    debug("templatesAmended", 0, ev);

//    ev.status == 'ok'

    let now = getCurrentDateTimeString();
    await mydb.adduser({id: amendId, created: now, code: amendCode, name: amendName, finger: amendFinger, rfid: amendRfid, pin: amendPin},
        async (row) => {

            debug("USER", 0, row);

            if ((row !== null) && (row.result === 0)) {
                debug("USER", 0, {message: 'ok'});
                resetAmendState();
            }  else {
                debug("USER", 1, {row: row});
                resetAmendState();
            }     

        }).catch(error => { 

            debug("USER", 0, {message: error});

            resetAmendState();

        });    

}

///////////////////////////////////////

//    PROCESS UI COMMANDS

///////////////////////////////////////

async function processCommandDeviceSerial() {
    
    tre.cmdGetRestDeviceAuth()
        .then(result => { 
            toBrowser(CMD_DEVICE_SERIAL_NO, 0, {serialNo: result.restDevice});
        })
        .catch(error => { 
            toBrowser(CMD_DEVICE_SERIAL_NO, 1, null);
        });
    
}

async function processCommandDeviceInformation() {

    debug("DeviceInformation", 0, mydb);

    tre.cmdGetRestDeviceAuth()
        .then(async (serial) => { 
/*            
            tre.cmdGetInetInfo()
                .then(async (ip) => { 
*/                    
                    con.cmdGetConfig()
                        .then(async (config) => { 
                        
                            debug("DEVICECONFIG", 0, config);

                            tre.cmdGetCommitInfo()
                                .then(async (commit) => { 
                        
                                    await mydb.getversion(
                                        (ver) => {
                                            if (ver) {
                                                toBrowser(CMD_DEVICE_INFORMATION, 0, {
                                                    serialNo: serial.restDevice, 
                                                    appVersion: formatMTVer(ver.ver), 
                                                    sysVersion: config.softwareVersion, 
                                                    ipConfig: (config.dhcp === true ? "DYNAMIC" : "STATIC"), 
                                                    ipAddress: (config.dhcp === true ? "" : config.ipaddress), // ip.ip),
                                                    subnetMask: (config.dhcp === true ? "" : config.subnetmask), // ip.mask),
                                                    dnsServer: (config.dhcp === true ? "" : config.dnsserver1), // ip.gatewayip),
                                                    dhcpServer: "",
                                                    githubBranch: commit.branch,
                                                    commitNumber: commit.commitid,
                                                    dateTime: getCurrentDateTimeString(),
                                                    lastUpdateDate: ver.stamp,
                                                    serverUrl: config.bkendUrl,
                                                    timezone: config.timeZone
                                                });
                                        
                                            }  else {
                                                fail(CMD_DEVICE_INFORMATION, "Error get version");
                                            }  

                                        }).catch(error => { 
                                            // problem
                                            fail(CMD_DEVICE_INFORMATION, "Error get schema ver " + error.toString());
                                        });  

                                }).catch(error => { 
                                    // problem
                                    fail(CMD_DEVICE_INFORMATION, "Error get commit info " + error.toString());
                                });  

                        }).catch(error => { 
                            // problem
                            fail(CMD_DEVICE_INFORMATION, "Error get config " + error.toString());
                        });  
/*
                }).catch(error => { 
                    // problem
                    fail(CMD_DEVICE_INFORMATION, "Error get ip address " + error.toString());
                });  
*/
        })
        .catch(error => { 
            fail(CMD_DEVICE_INFORMATION, "Error device auth " + error);
        });

}

async function processCommandDeviceRestart() {

    toBrowser(CMD_DEVICE_RESTART, 0, {delay: TIMEOUT_RESTART});
 
    clearTimeout(mtTimeout); 
    
    mtTimeout = setTimeout(
        async () => {
            disp.sysopReboot();
        }, Math.abs(TIMEOUT_RESTART) * 1000);
        
}
   
async function processCommandEmployeesList(ev) {

    await mydb.getusers(
        (rows) => {
            if ((rows !== null) && (rows.result === 0)) {
                toBrowser(CMD_EMPLOYEES_LIST, 0, {employees: rows.data});
            } else {
                if (state == STATE_ENROL)
                    enrolFail("Error retrieving users");
                else if (state == STATE_AUTH)
                    authFail("Error retrieving users");
                else if (PUNCH_STATES.includes(state))
                    punchFail("Error retrieving users");
                else
                    fail(CMD_EMPLOYEES_LIST, "Error state " + state);
            }          
        }).catch(error => { 
            if (state == STATE_ENROL)
                enrolFail("Error retrieving db users");
            else if (state == STATE_AUTH)
                authFail("Error retrieving db users");
            else if (PUNCH_STATES.includes(state))
                punchFail("Error retrieving db users");
            else
                fail(CMD_EMPLOYEES_LIST, "Error retrieving db users");
        });   

}
  
async function processCommandPunches(ev) {

    await mydb.getpunches(50,
        (rows) => {
            if ((rows !== null) && (rows.result === 0)) {
                toBrowser(CMD_PUNCHES, 0, {punches: rows.data});
            }  else {
                toBrowser(CMD_PUNCHES, 1, null);
            }
        }).catch(error => { 
            fail(CMD_PUNCHES, "Error retrieving punches");
        });   

}

async function processCommandPunchIn() {
    resetState();
    state = STATE_PUNCH_IN;
}
    
async function processCommandPunchOut() {
    resetState();
    state = STATE_PUNCH_OUT;
}
    
async function processCommandBreakStart() {
    resetState();
    state = STATE_BREAK_START;
}
    
async function processCommandBreakEnd() {
    resetState();
    state = STATE_BREAK_END;
}
    
async function processCommandPunchCancel() {
    resetState();
}

async function processCommandPinLogin(ev) {

    debug("PINLOGIN", 0, ev);

    if (PUNCH_STATES.includes(state) || (state == STATE_AUTH)) {

        await mydb.getuserbypin(ev.data.id, ev.data.pin, 
            async (row) => {
                
                debug("PINLOGIN", 0, {row: row});

                if ((row !== null) && (row.result === 0)) {

                    if (PUNCH_STATES.includes(state)) {

                        let now = getCurrentDateTimeString();

                        await mydb.addpunch(row.data.id, now, 'PIN', state, null, 
                            async (res) => {
                                if ((res !== null) && (res.result === 0)) {
                                             
                                    debug(state, 0, {response: res});

                                    toBrowser(state, 0, {id: row.data.id, code: row.data.code, name: row.data.name, datetime: now});

                                    postAttendance(res.data.id, {
                                        id: row.data.id,
                                        punch: now,
                                        method: 'PIN'
                                    });

                                }  else {

                                    punchFail("Error adding punch");

                                }   

                            }).catch(error => { 

                                punchFail("Exception adding punch");

                            });                

                    } else {

                        toBrowser(CMD_AUTH, 0, {
                            id: row.data.id,
                            code: row.data.code,
                            name: row.data.name,
                            method: 'PIN'
                        });

                        resetState();
                    }
                    
                } else {

                    if (PUNCH_STATES.includes(state))
                        punchFail("Error incorrect pin");
                    else
                        authFail("Error incorrect login");

                }          
            }

        ).catch(error => { 
            // problem
            fail(CMD_PIN_LOGIN, "Error retrieving db user");
        });          

    } else if ((state == STATE_ENROL) && (enrolState == ENROLSTATE_PIN)) {
    
        enrolPin = ev.data.pin;

        enrolNextStage();

    } else {
        fail(CMD_PIN_LOGIN, "Error state " + state);
    }
}

async function processCommandAuth() {
    resetState();
    state = STATE_AUTH;
}
    
async function processCommandEnrol(ev) {

    debug("processcommandenrol", 0, ev);

    resetState();
    
    state = STATE_ENROL;

    enrolId = ev.data.id;
    enrolName = ev.data.name;
    enrolCode = ev.data.code;
    enrolMethods.fingerprint = ev.data.methods.fingerprint;
    enrolMethods.rfid = ev.data.methods.rfid;
    enrolMethods.pin = ev.data.methods.pin;
    enrolMethods.photo = ev.data.methods.photo;

    await mydb.getuserbyid(enrolId,
        async (row) => {
            if ((row !== null) && (row.result === 0)) {
                // problem - already exists                    
                enrolFail("Error user already exists");
            }  else {

                if (enrolFingerCount < enrolMethods.fingerprint) {

                    toBrowser(CMD_ENROL_FINGERPRINT, 0, {index: enrolFingerCount + 1});
            
                } else if (enrolMethods.rfid === true) {
            
                    toBrowser(CMD_ENROL_RFID, 0, null);
            
                } else if (enrolMethods.pin === true) {
            
                    toBrowser(CMD_ENROL_PIN, 0, null);
            
                }
            }                                  
        }).catch(error => { 
            // problem
            enrolFail("Error retrieving db users");
        });   
    
}

async function processCommandEnrolFingerprintConfirm() {
    
    if ((state === STATE_ENROL) && (enrolState === null)) {

        enrolState = ENROLSTATE_FINGER;
        enrolFingerCount++;
        enrolFingerStage = 1;

        await disp.sendEvent(
            {evtype: disp.evtype.FpCmdEnroll, evdata:{templateid: enrolId.toString() + '#' + (enrolFingerCount - 1).toString()}}
        );

        toBrowser(CMD_ENROL_FINGERPRINT_1, 0, null);

    }

}
    
async function processCommandEnrolRfidConfirm() {

    if ((state === STATE_ENROL) && (enrolState === null)) {

        enrolState = ENROLSTATE_RFID;

    }
    
}
    
async function processCommandEnrolPinConfirm() {
    
    if ((state === STATE_ENROL) && (enrolState === null)) {

        enrolState = ENROLSTATE_PIN;

    }
}
    
async function processCommandEnrolCancel() {
    resetState();
}

async function processCommandBackup() {
}

async function processCommandRestore() {
}

async function processPing() {
    toBrowser(CMD_PING, 0, {stamp: getCurrentDateTimeString()});
}

async function processCommandDebug() {
    DEBUG_ON = !DEBUG_ON;
}


async function processCommand(ev) {

    debug("processcommand", 0, ev);

    switch (ev.evdata.cmd) {
        case CMD_DEVICE_SERIAL_NO: 
            await processCommandDeviceSerial();
        break;
        
        case CMD_DEVICE_INFORMATION: 
            await processCommandDeviceInformation();
        break;
            
        case CMD_DEVICE_RESTART:
            await processCommandDeviceRestart();
        break;
            
        case CMD_EMPLOYEES_LIST:
            await processCommandEmployeesList(ev.evdata);
        break;
        
        case CMD_PUNCHES:
            await processCommandPunches(ev.evdata);
        break;
        
        case CMD_PUNCH_IN:
            await processCommandPunchIn();
        break;
        
        case CMD_PUNCH_OUT:
            await processCommandPunchOut();
        break;
        
        case CMD_BREAK_START:
            await processCommandBreakStart();
        break;
        
        case CMD_BREAK_END:
            await processCommandBreakEnd();
        break;
        
        case CMD_PUNCH_CANCEL:
            await processCommandPunchCancel();
        break;
        
        case CMD_PIN_LOGIN:
            await processCommandPinLogin(ev.evdata);
        break;
        
        case CMD_AUTH:
            await processCommandAuth();
        break;
        
        case CMD_ENROL:
            await processCommandEnrol(ev.evdata);
        break;
        
        case CMD_ENROL_FINGERPRINT_CONFIRM:
            await processCommandEnrolFingerprintConfirm();
        break;
        
        case CMD_ENROL_RFID_CONFIRM:
            await processCommandEnrolRfidConfirm();
        break;
        
        case CMD_ENROL_PIN_CONFIRM:
            await processCommandEnrolPinConfirm();
        break;
        
        case CMD_ENROL_CANCEL:
            await processCommandEnrolCancel();
        break;

        case CMD_BACKUP:
            await processCommandBackup();
        break;

        case CMD_RESTORE:
            await processCommandRestore();
        break;

        case CMD_DEBUG:
            await processCommandDebug();
        break;

        case CMD_PING:
            await processPing();
        break;

    }
}

async function initPump() {
    bkend.pumpInterval(60);
}

// Application state machine, exported singleton
self = {
    identify:  async (ev) => {
        await processIdentify(ev);
    },
    
    localServer: async (ev) => {
        await processCommand(ev);
    },
    
    fpEnroll: async (ev) => {
        await processEnrol(ev);
    },

    fpData: async (ev) => {
        await processEnrolFingerData(ev);
    },

    fpDeleted: async (ev) => {
        debug("FPDELETED", 0, ev);
    },
    
    fpOpCompleted: async (ev) => {
        debug("FPOPCOMPLETED", 0, ev);
    },

    fpImportTemplate: async (ev) => {
        templateImported(ev);
    },

    fpAmendTemplates: async (ev) => {
        templatesAmended(ev);
    },

    camExposure: async (ev) => {
        // handle incomming pict
        console.log(TRACEHDR + "Processing cam exposure, post attendance: ", g.attOnExposure);
        expPhoto = ev.evdata.blob;
/*        if (g.attOnExposure)
            postAtt(g.specialId);*/
    },

    pumpEvents: async (evlist) => {
        // handle incomming pump events list
        if (evlist && evlist.payload) {
            evlist.payload.forEach(
                (ev) => {
                    handlePumpEvent(ev);
                }
            )
        }
    },

    sysop: async (ev) => {
    },
    
    commStatus: async (netw, firstcon) => {
        if (netw == "connected")
            console.log(TRACEHDR + "Network connected, first ", firstcon);
        if (netw == "disconnected")
            console.log(TRACEHDR + "Network disconnected");
    },

    processUiCommand: async (ev) => {
        await processCommand(ev);
    },
 
    setup: (wrap, mydb) =>  {
        // called on require
        console.log(TRACEHDR + "FSM setup");
        // save tla handle and eventhandlers to use here
        global.tla = wrap.tla;
        wrap.eventhandlers(wrap.tla, self);
        // globalize handlers for use here
        global.disp = wrap.disp;
        global.bkend = wrap.bkend;
        global.mydb = mydb;
        // open and activate db
        mydb.setupdb(wrap);
    },

    init: () => {
        // called once from event dispatcher
        console.log(TRACEHDR + "init");
    },

    browserAppState: () => {
        console.log(TRACEHDR + "Browser app active");
    },

    start: async () => {
        // called explicitly by startup
        bkend.pumpInterval(10);
        console.log(TRACEHDR + "State machine started");
    },
    
    tick: () => {
    },

    error: (err) => {
        console.log(TRACEHDR + " Error event", err);
    },
} // eo self

module.exports = (wrap, mydb) => {
    self.setup(wrap, mydb);
    return self;
}
